#!/usr/bin/perl
#
# Copyright (c) 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.010;
use strict;
use warnings;

use Test::More;
use Test::Command;

use lib 't/lib';
use Test::StalePID;

my $sp = Test::StalePID->new;
my $prog = $sp->test_prog;
my $version_line;
my @usage_lines;

plan tests => 8;

# A single version line with -V
subtest 'Version output with -V' => sub {
	$version_line = $sp->version_line;
};

# More than one usage line with -h
subtest 'Usage output with -h' => sub {
	@usage_lines = $sp->usage_lines;
};

subtest 'Usage and version output with -V -h' => sub {
	my @lines = $sp->get_ok_output('-V -h', '-V', '-h');
	is scalar @lines, 1 + @usage_lines, '-V -h output one more line than the usage message';
	is $lines[0], $version_line, '-V -h output the version line first';
	shift @lines;
	is_deeply \@lines, \@usage_lines, '-V -h output the usage message';
};

subtest 'Usage and version output with -hV' => sub {
	my @lines = $sp->get_ok_output('-hV', '-hV');
	is scalar @lines, 1 + @usage_lines, '-hV output one more line than the usage message';
	is $lines[0], $version_line, '-hV output the version line first';
	shift @lines;
	is_deeply \@lines, \@usage_lines, '-hV output the usage message';
};

subtest 'Long-form version' => sub {
	my @lines = $sp->get_ok_output('--version', '--version');
	is scalar @lines, 1, '--version output a single line';
	is $lines[0], $version_line, '--version output the version information';
};

subtest 'Long-form usage' => sub {
	my @lines = $sp->get_ok_output('--help', '--help');
	ok @lines > 1, '--help output more than one line';
	is_deeply \@lines, \@usage_lines, '--help output the usage information';
};

subtest 'Invalid short option' => sub {
	my @lines = $sp->get_error_output('invalid short option -X', '-X', 'Makefile');
	is scalar @lines, 1 + scalar @usage_lines, '-X output one more line than the usage message';
	shift @lines;
	is_deeply \@lines, \@usage_lines, '-X output the usage message';
};

subtest 'Invalid long option' => sub {
	my @lines = $sp->get_error_output('invalid short option --whee', '--whee', 'Makefile');
	is scalar @lines, 1 + scalar @usage_lines, '--whee output one more line than the usage message';
	shift @lines;
	is_deeply \@lines, \@usage_lines, '--whee output the usage message';
};
