#!/usr/bin/perl
#
# Copyright (c) 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.010;
use strict;
use warnings;

use File::Basename qw(basename);
use File::Temp;
use Test::More;
use Test::Command;

use lib 't/lib';

use Test::StalePID;

plan tests => 11;

my $sp = Test::StalePID->new;
my $prog = $sp->test_prog;
my @usage_lines;
my $cmd = $sp->get_cmd_ours;
my $d = $sp->tempdir;
my $procpath = "$d/proc";

# Get the usage lines.
subtest 'Usage output with -h' => sub {
	@usage_lines = $sp->usage_lines;
};

$sp->test_args('-p', '-P', $procpath);

for my $num (0, 1, 3, 4) {
	subtest "Fail with $num arguments specified" => sub {
		my $c = $sp->run(($prog) x $num);
		$c->exit_isnt_num(0, "failed with $num arguments");
		$c->stdout_is_eq('', "did not output anything with $num arguments");
		my @lines = split /\n/, $c->stderr_value;
		is_deeply \@lines, \@usage_lines, "output the usage message with $num arguments";
	};
}

mkdir $procpath, 0700 or BAIL_OUT "Could not create the $procpath directory: $!\n";
mkdir "$procpath/$$" or BAIL_OUT "Could not create the $procpath/$$ directory: $!\n";

my @files = (
	['comm', "$cmd", ''],
	['status', "Name: $cmd", "\nSomething else\n"],
	['status', "$cmd $$ something else", "\n"],
);

for my $spec (@files) {
	my ($cfname, $first, $rest) = @{$spec};
	my $cfpath = "$procpath/$$/$cfname";

	open my $cf, '>', $cfpath or BAIL_OUT "Could not create the $cfpath file: $!\n";
	print $cf "$first$rest" or BAIL_OUT "Could not write to the $cfpath file: $!\n";
	close $cf or BAIL_OUT "Could not close the $cfpath file: $!\n";

	# OK, let's build a string that is certainly longer than our process name.
	subtest 'Delete a temporary file, another program running' => sub {
		$sp->test_delete_if_different("another program, $cfname, $first");
	};

	subtest 'Leave it alone with the program still running' => sub {
		$sp->test_keep_if_same("same program, $cfname, $first");
	};

	unlink $cfpath or BAIL_OUT "Could not remove the $cfpath file: $!\n";
}
