#!/bin/sh
#
# Copyright (c) 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

package Test::StalePID;

use v5.010;
use strict;
use warnings;

use File::Temp;
use Test::Command;

use version; our $VERSION = version->declare("0.1.0_1");

our @ISA = ();

sub new
{
	my $proto = shift;
	my $class = ref $proto || $proto;
	my $self = {
		cmd_long	=> undef,
		cmd_ours		=> undef,
		tempdir		=> undef,
		test_args	=> [],
		test_prog	=> undef,
		version_line	=> undef,
	};
	bless $self, $class;
	return $self;
}

sub bail_out($)
{
	my ($reason) = @_;

	Test::More::BAIL_OUT($reason);
}

sub tempdir($)
{
	my ($self) = @_;

	if (!defined $self->{tempdir}) {
		$self->{tempdir} = File::Temp->newdir or
		    bail_out 'Could not create a temporary directory';
	}

	$self->{tempdir}
}

sub test_prog($)
{
	my ($self) = @_;

	if (!defined $self->{test_prog}) {
		my $prog = $ENV{TEST_PROG} || './stalepid';
		if (! -f $prog) {
			bail_out "The test program '$prog' does not exist";
		}
		if (! -x $prog) {
			bail_out "The test program '$prog' is not executable";
		}
		$self->{test_prog} = $prog;
	}

	$self->{test_prog}
}

sub test_args($ @)
{
	my ($self, @args) = @_;

	if (@args) {
		push @{$self->{test_args}}, @args;
	}

	@{$self->{test_args}}
}

sub run($ @)
{
	my ($self, @args) = @_;

	Test::Command->new(cmd => [$self->test_prog, $self->test_args, @args]);
}

sub get_ok_output($ $ @) {
	my ($self, $desc, @args) = @_;

	my $c = $self->run(@args);
	$c->exit_is_num(0, "$desc succeeded");
	$c->stderr_is_eq('', "$desc did not output any errors");
	split /\n/, $c->stdout_value
}

sub get_error_output($ $) {
	my ($self, $desc, @args) = @_;

	my $c = $self->run(@args);
	$c->exit_isnt_num(0, "$desc failed");
	$c->stdout_is_eq('', "$desc did not output anything");
	split /\n/, $c->stderr_value
}

sub version_line($)
{
	my ($self) = @_;

	if (!defined $self->{version_line}) {
		my $c = $self->run('-V');
		$c->exit_is_num(0, '-V succeeded');
		$c->stderr_is_eq('', '-V did not output any errors');
		my @lines = split /\n/, $c->stdout_value, -1;
		bail_out 'Unexpected number of lines in the -V output' unless @lines == 2;
		bail_out 'Unexpected -V line' unless $lines[0] =~ /^ stalepid \s \S+ $/x;
		$self->{version_line} = $lines[0];
	}

	$self->{version_line}
}

sub usage_lines($)
{
	my ($self) = @_;

	if (!defined $self->{usage_lines}) {
		my $c = $self->run('-h');
		$c->exit_is_num(0, '-h succeeded');
		$c->stderr_is_eq('', '-h did not output any errors');
		my @lines = split /\n/, $c->stdout_value;
		bail_out 'Too few lines in the -h output' unless @lines > 1;
		bail_out 'Unexpected -h first line' unless $lines[0] =~ /^ Usage: \s+ stalepid /x;
		$self->{usage_lines} = \@lines;
	}

	@{$self->{usage_lines}}
}

sub get_cmd_ours($)
{
	my ($self) = @_;

	if (!defined $self->{cmd_ours}) {
		my $pid = $$; # do not let Perl's fork() fool us
		open my $ps, '-|', 'ps', '-c', '-o', 'command', '-p', $pid or bail_out "Could not fork for ps: $!";
		my $header = <$ps>;
		bail_out 'ps on ourselves did not output even a single line' unless defined $header;
		my $cmd = <$ps>;
		bail_out 'ps on ourselves did not output a second line' unless defined $cmd;
		chomp $cmd;
		close $ps or bail_out 'ps on ourselves did not complete successfully';
		$self->{cmd_ours} = $cmd;
	}

	$self->{cmd_ours}
}

sub get_cmd_too_long($)
{
	my ($self) = @_;

	if (!defined $self->{cmd_long}) {
		my $long = join '..', $0, @ARGV;
		$self->{cmd_long} = $long x 2;
	}

	$self->{cmd_long}
}

sub create_pidfile($ $)
{
	my ($self, $fname) = @_;

	open my $outf, '>', $fname or bail_out "Could not create $fname: $!";
	say $outf $$;
	close $outf or bail_out "Could not close $fname after writing: $!";
	Test::More::ok -f $fname, "successfully created $fname";
}

sub test_delete_if_different($ $ $)
{
	my ($self, $desc) = @_;
	my $fname = $self->tempdir.'/too-long.pid';

	$self->create_pidfile($fname);
	my @lines = $self->get_ok_output($desc, $fname, $self->get_cmd_too_long);
	Test::More::is @lines, 0, "$desc: did not output anything";
	Test::More::ok ! -f $fname, "$desc: deleted $fname";
}

sub test_keep_if_same($ $ $)
{
	my ($self, $desc) = @_;
	my $fname = $self->tempdir.'/ourselves.pid';
	my $cmd = $self->get_cmd_ours;

	$self->create_pidfile($fname);
	my @lines = $self->get_ok_output($desc, $fname, $self->get_cmd_ours);
	Test::More::is @lines, 0, "$desc: output nothing";
	Test::More::ok -f $fname, "$desc: left $fname alone";
}

1;
