# Copyright (c) 2005, 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

PROG=		stalepid
SRC=		stalepid.pl
MAN1=		stalepid.1

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/share/man/man

MAN1Z=		${MAN1}.gz

MKDIR?=		mkdir -p
COMPRESS_CMD?=	gzip -cn9
RM?=		rm -f
MV?=		mv -f

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

SHAREOWN?=	root
SHAREGRP?=	root
SHAREMODE?=	644

INSTALL_DATA?=	install -c -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}
INSTALL_SCRIPT?=	install -c -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_EXEC?=	install -m ${BINMODE}

TEST_PROG?=	./${PROG}

all:		${PROG} ${MAN1Z}

${PROG}:	${SRC}
		${INSTALL_EXEC} ${SRC} ${PROG}

${MAN1Z}:	${MAN1}
		${COMPRESS_CMD} ${MAN1} > ${MAN1Z}.tmp || ${RM} ${MAN1Z}.tmp
		${MV} ${MAN1Z}.tmp ${MAN1Z}

test:		${TEST_PROG} test-harness
		env TEST_PROG='${TEST_PROG}' prove t

test-harness:
		@if ! prove t/harness > /dev/null 2>&1; then \
			echo '' 1>&2; \
			echo 'The test harness sanity check failed' 1>&2; \
			echo 'Please make sure that the prove tool, Perl, and the Test::Command Perl module' 1>&2; \
			echo '(libtest-command-perl, perl-Test-Command, p5-Test-Command) are installed' 1>&2; \
			echo '' 1>&2; \
			false; \
		fi

install:	all
		-${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_SCRIPT} ${PROG} ${DESTDIR}${BINDIR}/
		-${MKDIR} ${DESTDIR}${MANDIR}1
		${INSTALL_DATA} ${MAN1Z} ${DESTDIR}${MANDIR}1/

clean:
		${RM} ${PROG} ${MAN1Z} ${MAN1Z}.tmp

.PHONY:		all test test-harness install clean
